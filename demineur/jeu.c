// Démineur par Informatique 1ère année groupe B

// Veuillez utiliser la librairie fournie dans l'archive, je l'ai modifiée pour mettre un titre à ma fenêtre :)
// Indenté avec la commande indent sous Debian
// L'usage de la console est facultatif, mais il permet de se rendre compte du fonctionnement du jeu
// Un clic droit permet de marquer une case, un clic gauche de dévoiler une case. Vous pouvzez modifier la largeur de l'écran, le nombre de mines,
//et le nombre de mines max qu'il est possible d'exploser

//J'ai commencé la fonction autozero, je n'ai pas réussi à la terminer...mon système de tableaux de points a rendu les choses compliquées




// Le 10/10/15

#include "../lib/libgraphique.h"
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

//1. directives au preprocesseur et variables globales

#define LARGEUR_ECRAN 500
#define HAUTEUR_ECRAN LARGEUR_ECRAN
#define NOMBRE_MINES 10		// Maximum: ((LARGEUR_ECRAN/LARGEUR_CASE)*(LARGEUR_ECRAN/LARGEUR_CASE))
#define LARGEUR_CASE 20		// Largeur de la case, en pixels (ne pas modifier, la largeur des images, elle, ne bougera certainement pas)
#define MINES_EXP_MAX 3		// Nombre maximum de mines explosées avant de perdre (3 <=> Jeu perdu à la 3ème mine explosée)
#define TRUE 1 // Ne pas modifier TRUE et FALSE, le jeu marcherait moins bien
#define FALSE 0

//2. déclaration des fonctions

void quadrillage ();		// Cette fonction permet de dessiner la grille du démineur
void mines (int n);		// Cette fonction créé un tableau contenant les coordonnées des cases avec une mine et un tableau avec les coordonnées des cases sans mine
int fonc_bombes_proximite (Point coup_joueur);
int fonc_bombes_proximite_autozero (Point coup_joueur);
void autozero (Point coup_joueur);	// Permet de découvrir automatiquement les cases avec 0 mines à proximité
void debug ();			// Fonction de deboggage qui permet d'afficher les mines
void splash_screen ();



//3. Tableaux (globaux) ou sont stockés l'emplacement des cases et l'état du jeu

Point tableau_cases_decouvertes[LARGEUR_ECRAN / LARGEUR_CASE] = { {0} };	// Tableau contenants les coups 'gagnants': terrain déminé ou case vide révélée
Point tableau_cases_deminees[NOMBRE_MINES] = { {0} };	// Tableau contenants le cases déminées

Point tableau_cases_jeu[((LARGEUR_ECRAN / LARGEUR_CASE) * (LARGEUR_ECRAN / LARGEUR_CASE))];	// Tableau contenant les coordonnées de toutes les cases du jeu
Point tableau_cases_explosees[NOMBRE_MINES];	// Tableau contenant les coordonnées des coups 'perdus': bombe explosée
Point tableau_cases_minees[NOMBRE_MINES];	// Tableau contenant les coordonées des mines du jeu
Point tableau_cases_vides[(((LARGEUR_ECRAN / LARGEUR_CASE) * (LARGEUR_ECRAN / LARGEUR_CASE)) - NOMBRE_MINES)];	// Tableau contenant les coordonnées des cases vides


//4. définition de la fonction main

int
main (void)
{

  ouvrir_fenetre (LARGEUR_ECRAN, HAUTEUR_ECRAN);	// Ouverture de la fenêtre de jeu
  splash_screen (); // On affiche l'écran de démarrage
  actualiser ();
  attendre_clic ();
  quadrillage ();		// Dessin de la grille
  mines (NOMBRE_MINES);		// On place les mines dans la partie!
  actualiser ();		// On affiche la grille
  // debug (); //décommenter le débug pour afficher les bombes


//////////////////////////////////
//// La partie peut commencer ////
/////////////////////////////////

  printf ("Partie commencée avec %d mines. Bonne chance!\n", NOMBRE_MINES);

  // Déclaration des variables utiles à la partie

  int partie_en_cours = TRUE;	// Lance le while principal
  int mines_explosees = 0;	// Le compteur de bombes exploseés est initialisé à 0
  int bombes_proximite;
  int bombes_proximite_int;
  char bombes_proximite_char[1];
  Point emplacement_texte;
  Point coup_joueur;
  int i = 0;


// Boucle principale du jeu

  while (partie_en_cours)
    {
      i = 0;
      //On attend que le joueur clique sur une case
      coup_joueur = attendre_clic_gauche_droite ();
      coup_joueur.x = coup_joueur.x - (coup_joueur.x % LARGEUR_CASE);
      coup_joueur.y = coup_joueur.y - (coup_joueur.y % LARGEUR_CASE);

      if ((coup_joueur.x >= 0) && (coup_joueur.y >= 0))
	{
	  i = 0;
	  // On teste si il sagit d'une mine déjà explosée, ou si la case contient une mine
	  while (i <= NOMBRE_MINES)
	    {
	      if ((coup_joueur.x == tableau_cases_explosees[i].x) && (coup_joueur.y == tableau_cases_explosees[i].y))
		{
		  printf ("Bombe déjà explosée nb:%d\n", mines_explosees);
		  break;
		}
	      else if ((coup_joueur.x == tableau_cases_minees[i].x) && (coup_joueur.y == tableau_cases_minees[i].y))
		{
		  mines_explosees++;	// On incrémente le compteur de bombes explosées de 1
		  tableau_cases_explosees[i].x = coup_joueur.x;	// On ajoute les coordonnées de la case explosée au tableau_cases_explosees
		  tableau_cases_explosees[i].y = coup_joueur.y;
		  afficher_image ("case_clicked_bomb.bmp", coup_joueur);	// On affiche l'image de la mine
		  printf ("Nouveau perdu: %d,%d\n",tableau_cases_explosees[i].x,tableau_cases_explosees[i].y);
		  actualiser ();
		  break;
		}
	      else
		{
		  i++;		// On passe à la case suivante
		}
	    }

	  i = 0;		// On remet la variable de boucle à zéro

	  // On teste si le terrain est déjà déminé, et si ce n'est pas le cas on le démine!

	  while (i <= ((LARGEUR_ECRAN / LARGEUR_CASE) * (LARGEUR_ECRAN / LARGEUR_CASE)))
	    {

	      if ((coup_joueur.x == tableau_cases_decouvertes[i].x) && (coup_joueur.y == tableau_cases_decouvertes[i].y))
		{
		  break;
		}
	      else if ((coup_joueur.x == tableau_cases_vides[i].x) && (coup_joueur.y == tableau_cases_vides[i].y))
		{
		  if (i >= (((LARGEUR_ECRAN / LARGEUR_CASE) * (LARGEUR_ECRAN / LARGEUR_CASE)) - NOMBRE_MINES))	// Le tableau tableau_cases_vides est égal à (toutes les cases du jeu)-(le nombres de mines), cette condition
		    {		// est donc nécessaire pour ne pas lire en dehors du tableau
		      break;
		    }
		  else
		    {
		      tableau_cases_decouvertes[i].x = coup_joueur.x;	// Le joueur a révélé une case qui ne contient pas de mine, on peut l'ajouter au tableau_cases_decouvertes
		      tableau_cases_decouvertes[i].y = coup_joueur.y;

		      afficher_image ("case_clicked_nobomb.bmp", coup_joueur);	// On remplace l'image de la case pour montrer qu'elle est déjà déminée
		      bombes_proximite = fonc_bombes_proximite (coup_joueur);	// On calcule le nombre de bombes à proximité de la case
          bombes_proximite_int = bombes_proximite;
		      sprintf (bombes_proximite_char, "%d", bombes_proximite_int);	// On convertit le int en char

		      emplacement_texte.x = coup_joueur.x + 5;	// On définit l'emplacement du nombre
		      emplacement_texte.y = coup_joueur.y;

		      afficher_texte (bombes_proximite_char, 15, emplacement_texte, blanc);	// On place le nombre de bombes à proximité dans la case

		      printf ("\nNouveau gagne: %d,%d\n",tableau_cases_decouvertes[i].x,tableau_cases_decouvertes[i].y);
		      printf ("(Bombes à proximité: %d)\n",bombes_proximite);

		      if (bombes_proximite == 0)
			{
			  printf ("Autozero\n");
        actualiser();
			  //autozero (coup_joueur); Appel de la fonction autozero si la fontion autozero marchait
			}
		      actualiser ();	// On actualise le tout
		      break;
		    }
		}
	      else
		{
		  i++;
		}
	    }



	}

      else			// si clic droit
	{
	  coup_joueur.x = coup_joueur.x * (-1);	// On remet les coordonnées en positif
	  coup_joueur.y = coup_joueur.y * (-1);
	  i = 0;
	  while (i <=((LARGEUR_ECRAN / LARGEUR_CASE) *(LARGEUR_ECRAN / LARGEUR_CASE)))
	    {
	      if (i <= NOMBRE_MINES)
		{
		  if ((coup_joueur.x == tableau_cases_explosees[i].x) && (coup_joueur.y == tableau_cases_explosees[i].y))
		    {

		      printf ("Bombe déjà explosée nb:%d\n",mines_explosees);
		      break;
		    }

		  else if ((coup_joueur.x == tableau_cases_minees[i].x) && (coup_joueur.y == tableau_cases_minees[i].y))
		    {
		      afficher_image ("case_maybe.bmp", coup_joueur);
		      actualiser ();
		      break;
		    }

		  else if ((coup_joueur.x == tableau_cases_decouvertes[i].x) && (coup_joueur.y == tableau_cases_decouvertes[i].y))
		    {
		      break;
		    }

		  else if ((coup_joueur.x == tableau_cases_vides[i].x) && (coup_joueur.y == tableau_cases_vides[i].y))
		    {
		      afficher_image ("case_maybe.bmp", coup_joueur);
		      actualiser ();
		      break;
		    }
		  else
		    {
		      i++;
		    }
		}

	      else if ((coup_joueur.x == tableau_cases_decouvertes[i].x) && (coup_joueur.y == tableau_cases_decouvertes[i].y))
		{
		  break;
		}

	      else if ((coup_joueur.x == tableau_cases_vides[i].x) && (coup_joueur.y == tableau_cases_vides[i].y))
		{
		  afficher_image ("case_maybe.bmp", coup_joueur);
		  actualiser ();
		  break;
		}
	      else
		{
		  i++;
		}




	    }



	}			// fin si clic droit



      if (mines_explosees >= MINES_EXP_MAX)
	{
	  partie_en_cours = FALSE; // Le while principal s'arrête
	}

    }				// fin partie en cours

  printf ("Vous avez perdu!\n");	// La partie est finie






  attendre_clic ();
  fermer_fenetre (); // On ferme le jeu
  return 0;
} // Fin boucle main

/////////////////////////////////////////////
/////////4.définition des fonctions/////////
/////////////////////////////////////////////



//Grille de jeu
void
quadrillage ()
{
  Point coin_case = { 0, 0 };
  while (coin_case.y <= HAUTEUR_ECRAN)
    {
      while (coin_case.x <= LARGEUR_ECRAN)
	{
	  afficher_image ("case.bmp", coin_case);
	  coin_case.x += LARGEUR_CASE;
	}
      coin_case.x = 0;
      coin_case.y += LARGEUR_CASE;
    }

}

//Fontion s'occupant de générer les bombes
void
mines (int n)
{

  Point temp;
  Point coin_mine = { 0, 0 };
  int i = 0, j;


  // Tableau contenant toutes les coordonnées des cases du jeu
  while (coin_mine.y < HAUTEUR_ECRAN)
    {
      while (coin_mine.x < LARGEUR_ECRAN)
	{
	  tableau_cases_jeu[i].x = coin_mine.x;
	  tableau_cases_jeu[i].y = coin_mine.y;
	  coin_mine.x += LARGEUR_CASE;
	  i++;
	}

      coin_mine.x = 0;
      coin_mine.y += LARGEUR_CASE;
    }


  /*
     Implémentation de l'argorithme "The Fisher-Yates shuffle" de  Ronald Fisher et Frank Yates
     pour mélanger les coordonnées du tableau au hasard, et ainsi avoir un emplacement de bombes
     différent à chaque partie
   */
  // On mélange les coordonées des cases de la grille

  for (i = 0;
       i <
       (((LARGEUR_ECRAN / LARGEUR_CASE) * (LARGEUR_ECRAN / LARGEUR_CASE)) -
	1); i++)
    {
      // j est une variable aélatoire entre i et n-1
      j =
	i +
	rand () %
	(((LARGEUR_ECRAN / LARGEUR_CASE) * (LARGEUR_ECRAN / LARGEUR_CASE)) -
	 i);
      //On échange les valeurs de tab[i] et tab [j]
      temp = tableau_cases_jeu[i];
      tableau_cases_jeu[i] = tableau_cases_jeu[j];
      tableau_cases_jeu[j] = temp;
    }


  // On pioche un nombre défini de coordonées dans le tableau pour faire notre tableau de bombes
  for (i = 0; i < n; i++)
    {
      tableau_cases_minees[i].x = tableau_cases_jeu[i].x;
      tableau_cases_minees[i].y = tableau_cases_jeu[i].y;

    }
// On met les coordonnées qui restent dans un autre tableau qui contient les cases vides
  for (i = 0;
       i <
       (((LARGEUR_ECRAN / LARGEUR_CASE) * (LARGEUR_ECRAN / LARGEUR_CASE))) -
       n; i++)
    {
      tableau_cases_vides[i].x = tableau_cases_jeu[i + n].x;
      tableau_cases_vides[i].y = tableau_cases_jeu[i + n].y;
    }

}


// Cette fonction permet de connaître le nombre de bombes à proximité, en testant les 8 cases voisines
int
fonc_bombes_proximite (Point coup_joueur)
{
  int bombes_proximite;
  int k = 0;
  Point coup_joueur_droite;
  Point coup_joueur_gauche;
  Point coup_joueur_haut;
  Point coup_joueur_bas;
  Point coup_joueur_dhd;
  Point coup_joueur_dhg;
  Point coup_joueur_dbd;
  Point coup_joueur_dbg;

  coup_joueur_droite.x = coup_joueur.x + LARGEUR_CASE;
  coup_joueur_droite.y = coup_joueur.y;

  coup_joueur_gauche.x = coup_joueur.x - LARGEUR_CASE;
  coup_joueur_gauche.y = coup_joueur.y;

  coup_joueur_haut.x = coup_joueur.x;
  coup_joueur_haut.y = coup_joueur.y - LARGEUR_CASE;

  coup_joueur_bas.x = coup_joueur.x;
  coup_joueur_bas.y = coup_joueur.y + LARGEUR_CASE;

  coup_joueur_dhd.x = coup_joueur.x + LARGEUR_CASE;
  coup_joueur_dhd.y = coup_joueur.y - LARGEUR_CASE;

  coup_joueur_dhg.x = coup_joueur.x - LARGEUR_CASE;
  coup_joueur_dhg.y = coup_joueur.y - LARGEUR_CASE;

  coup_joueur_dbd.x = coup_joueur.x + LARGEUR_CASE;
  coup_joueur_dbd.y = coup_joueur.y + LARGEUR_CASE;

  coup_joueur_dbg.x = coup_joueur.x - LARGEUR_CASE;
  coup_joueur_dbg.y = coup_joueur.y + LARGEUR_CASE;


  while (k <= NOMBRE_MINES)
    {
      if ((coup_joueur_droite.x == tableau_cases_minees[k].x)
	  && (coup_joueur_droite.y == tableau_cases_minees[k].y))
	{
	  bombes_proximite += 1;
	}

      else if ((coup_joueur_gauche.x == tableau_cases_minees[k].x)
	       && (coup_joueur_gauche.y == tableau_cases_minees[k].y))
	{
	  bombes_proximite += 1;
	}

      else if ((coup_joueur_haut.x == tableau_cases_minees[k].x)
	       && (coup_joueur_haut.y == tableau_cases_minees[k].y))
	{
	  bombes_proximite += 1;
	}

      else if ((coup_joueur_bas.x == tableau_cases_minees[k].x)
	       && (coup_joueur_bas.y == tableau_cases_minees[k].y))
	{
	  bombes_proximite += 1;
	}

      else if ((coup_joueur_dhd.x == tableau_cases_minees[k].x)
	       && (coup_joueur_dhd.y == tableau_cases_minees[k].y))
	{
	  bombes_proximite += 1;
	}

      else if ((coup_joueur_dhg.x == tableau_cases_minees[k].x)
	       && (coup_joueur_dhg.y == tableau_cases_minees[k].y))
	{
	  bombes_proximite += 1;
	}

      else if ((coup_joueur_dbd.x == tableau_cases_minees[k].x)
	       && (coup_joueur_dbd.y == tableau_cases_minees[k].y))
	{
	  bombes_proximite += 1;
	}

      else if ((coup_joueur_dbg.x == tableau_cases_minees[k].x)
	       && (coup_joueur_dbg.y == tableau_cases_minees[k].y))
	{
	  bombes_proximite += 1;
	}

      k++;

    }

  return bombes_proximite;
}

// Même chose que la fonction du dessus, mais cette fois-ci on ne teste que 4 cases: gauche droite haut bas. Seules ces cases sont nécéssaires pour l'autozero

int
fonc_bombes_proximite_autozero (Point coup_joueur)
{

  int bombes_proximite_autozero;
  int k = 0;
  Point coup_joueur_droite;
  Point coup_joueur_gauche;
  Point coup_joueur_haut;
  Point coup_joueur_bas;

  coup_joueur_droite.x = coup_joueur.x + LARGEUR_CASE;
  coup_joueur_droite.y = coup_joueur.y;

  coup_joueur_gauche.x = coup_joueur.x - LARGEUR_CASE;
  coup_joueur_gauche.y = coup_joueur.y;

  coup_joueur_haut.x = coup_joueur.x;
  coup_joueur_haut.y = coup_joueur.y - LARGEUR_CASE;

  coup_joueur_bas.x = coup_joueur.x;
  coup_joueur_bas.y = coup_joueur.y + LARGEUR_CASE;


  while (k <= NOMBRE_MINES)
    {
      if ((coup_joueur_droite.x == tableau_cases_minees[k].x) && (coup_joueur_droite.y == tableau_cases_minees[k].y))
	{
	  bombes_proximite_autozero += 1;
	}

      else if ((coup_joueur_gauche.x == tableau_cases_minees[k].x) && (coup_joueur_gauche.y == tableau_cases_minees[k].y))
	{
	  bombes_proximite_autozero += 1;
	}

      else if ((coup_joueur_haut.x == tableau_cases_minees[k].x) && (coup_joueur_haut.y == tableau_cases_minees[k].y))
	{
	  bombes_proximite_autozero += 1;
	}

      else if ((coup_joueur_bas.x == tableau_cases_minees[k].x) && (coup_joueur_bas.y == tableau_cases_minees[k].y))
	{
	  bombes_proximite_autozero += 1;
	}

      k++;

    }

  return bombes_proximite_autozero;
}

// Fonction qui génère l'écran de démarrage du jeu
void
splash_screen ()		//Ecran de demarrage, la taille et l'emplacement du texte est automatique
{				//Le code est assez explicite sur son fonctionnement
  int taille = (LARGEUR_ECRAN * 15) / 200;
  Point p1;
  Point p2 = taille_texte ("Demineur | G.Monarque", taille);
  p1.x = LARGEUR_ECRAN - p2.x;
  p1.x = p1.x / 2;
  p1.y = LARGEUR_ECRAN / 2 - p2.y;
  afficher_texte ("Demineur | G.Monarque", taille, p1, blanc);


}

// Fonction autozero, qui découvre les cases automatiquement si les cases à proximité ne contiennent aucune bombe
// cela ne marchait que pour les cases de droite
void
autozero (Point coup_joueur)
{
  int bombes_proximite = 0;
  Point emplacement_texte;
  Point coup_joueur_autozero;
  coup_joueur_autozero.x = coup_joueur.x;
  coup_joueur_autozero.y = coup_joueur.y;

  char bombes_proximite_char[1];

  while (bombes_proximite == 0)
    {

      coup_joueur_autozero.x += 20;
      if (coup_joueur_autozero.x > LARGEUR_ECRAN)
      {
        break;
      }

      if (bombes_proximite == 0)
	{
	  bombes_proximite = fonc_bombes_proximite_autozero (coup_joueur_autozero);
	  emplacement_texte.x = coup_joueur_autozero.x + 5;	// On définit l'emplacement du texte dans la case
	  emplacement_texte.y = coup_joueur_autozero.y;
	   afficher_image ("case_clicked_nobomb.bmp", coup_joueur_autozero);
	  sprintf (bombes_proximite_char, "%d", bombes_proximite);	// On convertit le int en char
	  afficher_texte (bombes_proximite_char, 15, emplacement_texte, blanc);	//  On place le nombre de bombes à proximité dans la case
	  actualiser ();

	}

    }




}


// Fonction qui permet d'afficher les bombes pour vérifier le bon fonctionnement du jeu
void
debug ()
{
  int k;
  Point coin;
  for (k = 0; k < NOMBRE_MINES; k++)
    {
      coin.x = tableau_cases_minees[k].x;
      coin.y = tableau_cases_minees[k].y;
      afficher_image ("case_clicked_bomb.bmp", coin);
    }
  actualiser ();
}
